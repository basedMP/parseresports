local io = require("io")
while true do

    print()
    io.write("hoi4>")
    local input = io.read("*l")

    local func, err = load(input, "input.lua")
    if not func then
        print(err)
        goto next
    end

    local succ, err = pcall(func)
    if err ~= nil then
        print(err)
    end

    ::next::
end