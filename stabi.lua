local args = {...}
local hoi4 = require("hoi4")
local game = hoi4.parse(args[1])

local stabi = {}
for country in game.countries:iter() do
    stabi[#stabi+1] = {country.name, country.stability.value}
end

table.sort(stabi, function(a1, a2)
    return a1[2] > a2[2]
end)

for i,v in ipairs(stabi) do
    print(i, v[1], v[2])
end