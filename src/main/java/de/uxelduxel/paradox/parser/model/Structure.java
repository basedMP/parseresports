package de.uxelduxel.paradox.parser.model;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Structure extends AbstractToken implements TokenContainer {

    private List<Token> tokens = new ArrayList<>();
    private Map<String, Token> tokenMap = new HashMap<>();

    @Override
    public void compile(String prefix, Writer writer) throws IOException {


        //writer.write(getComment());
        writer.write(prefix);
        if (getName() != null) {
            writer.write(getName());
            writer.write(" = ");
        }

        writer.write("{\n");

        String nPrefix = "\t"+prefix;
        for (Token t : tokens) {
            t.compile(nPrefix, writer);
        }

        writer.write(prefix);
        writer.write("}\n \n");
    }

    @Override
    public TokenType type() {
        if (getParent() == null) {
            return TokenType.ROOT;
        }
        return getName() != null ? TokenType.SECTION : TokenType.ARRAY;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    @Override
    public Token get(int index) {
        if (index < 0 || index >= tokens.size()) {
            return null;
        }
        return tokens.get(index);

    }

    @Override
    public Token get(String name) {
        if (name == null) {
            return null;
        }
        return tokenMap.get(name);
    }

    @Override
    public void remove(Token token) {
        if (token == null) {
            return;
        }
        tokenMap.remove(token.getName(), token);
        tokens.remove(token);
    }

    @Override
    public Token remove(int index) {
        if (index < 0 || index >= tokens.size()) {
            return null;
        }

        Token old = tokens.remove(index);

        if (old != null) {
            tokenMap.remove(old.getName(), old);
        }



        return old;
    }

    @Override
    public Token set(int index, Token token) {
        if (token == null) {
            return remove(index);
        }

        if (index > tokens.size()) {
            add(token);
            return null;
        }

        if (index < 0) {
            return null;
        }

        Token old =  tokens.set(index, token);

        if (old != null) {
            tokenMap.remove(old.getName(), old);
        }

        return old;
    }

    public void add(Token child) {
        if (child == null) {
            return;
        }
        this.tokens.add(child);
        tokenMap.putIfAbsent(child.getName(), child);
    }
}
