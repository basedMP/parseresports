package de.uxelduxel.paradox.parser.model;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;

public class Literal extends AbstractToken {

    public Literal() {

    }

    @Override
    public void compile(String prefix, Writer writer) throws IOException {
        writer.write(prefix);
        writer.write(getName());
        writer.write('\n');
    }

    @Override
    public TokenType type() {
        return TokenType.LITERAL;
    }

}
