package de.uxelduxel.paradox.parser.model;

import java.util.Collection;
import java.util.List;

public interface TokenContainer extends Token {

    Collection<Token> getTokens();

    Token get(int index);

    Token get(String name);

    void remove(Token token);

    Token remove(int index);

    Token set(int index, Token token);

    void add(Token child);

}
