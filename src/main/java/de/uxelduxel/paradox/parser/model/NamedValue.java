package de.uxelduxel.paradox.parser.model;

import java.io.IOException;
import java.io.Writer;

public class NamedValue extends AbstractToken {



    private String value;

    public NamedValue() {

    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public void compile(String prefix, Writer writer) throws IOException {
        writer.write(prefix);
        writer.write(getName());
        writer.write(" = ");
        writer.write(value);
        writer.write('\n');
    }

    @Override
    public TokenType type() {
        return TokenType.NAMED_VALUE;
    }
}
