package de.uxelduxel.paradox.parser.model;

import java.util.Collection;

public abstract class AbstractToken implements Token {
    private String name;
    private TokenContainer parent;
    private String comment;

    @Override
    public TokenContainer getParent() {
        return parent;
    }

    @Override
    public void setParent(TokenContainer parent) {
        this.parent = parent;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getComment() {
        return comment;
    }

    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }


    @Override
    public String toString() {
        if (name == null) {
            return type().name();
        }
        return name + "@" + type();

    }


}
