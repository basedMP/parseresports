package de.uxelduxel.paradox.parser.model;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

public interface Token {

    TokenContainer getParent();
    void setParent(TokenContainer parent);

    String getName();
    void setName(String name);

    String getComment();
    void setComment(String comment);

    void compile(String prefix, Writer writer) throws IOException;
    TokenType type();

    enum TokenType {
        ARRAY,
        SECTION,
        LITERAL,
        NAMED_VALUE,
        ROOT,
    }

}
