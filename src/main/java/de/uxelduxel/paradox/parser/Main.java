package de.uxelduxel.paradox.parser;

import de.uxelduxel.paradox.parser.lua.ParadoxParserLib;
import io.github.alexanderschuetz97.luajsocket.lib.LuaJSocketLib;
import io.github.alexanderschuetz97.luajsocket.lib.MobDebugCompatibleDebugLib;
import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.lib.jse.JsePlatform;

import java.io.*;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            System.out.println("java -jar parser.jar <LUAFILE> <...>");
            System.exit(-1);
            return;
        }

        Globals globals = JsePlatform.standardGlobals();
        globals.load(new MobDebugCompatibleDebugLib());
        globals.load(new LuaJSocketLib());
        globals.load(new ParadoxParserLib());


        File f = new File(args[0]);
        if (!f.exists() || !f.isFile()) {
            System.out.println(f.getAbsolutePath() + " does not exist or is not a file");
            System.exit(-1);
            return;

        }

        LuaValue[] largs = new LuaValue[args.length-1];
        for (int i = 1; i < args.length; i++) {
            largs[i-1] = LuaValue.valueOf(args[i]);
        }

        globals.set("mobdebug", globals.load(new InputStreamReader(Main.class.getResourceAsStream("/mobdebug.lua")), "mobdebug.lua").call());

        globals.load(new InputStreamReader(new FileInputStream(f)), f.getName()).invoke(largs);
    }


}
