package de.uxelduxel.paradox.parser;

import de.uxelduxel.paradox.parser.model.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;

public class Parser {

    private enum ParsingState {
        LOOKING_FOR_END_OF_ESCAPE,
        LOOKING_FOR_END_OF_COMMENT,
        LOOKING_FOR_NAME,
        LOOKING_FOR_TOKEN_TYPE,
        LOOKING_FOR_END_OF_VALUE,
    }

    private final TokenContainer rootStructure = new Structure();

    private TokenContainer currentStructure = rootStructure;

    private NamedValue currentValue;

    private ParsingState currentState = ParsingState.LOOKING_FOR_NAME;

    private final LinkedList<ParsingState> stateStack = new LinkedList<>();

    private String currentComment = "";

    private String currentName = "";

    protected Parser() {
        //.
    }

    public void parseNext(char currentChar) {
        switch (currentState) {
            case LOOKING_FOR_END_OF_ESCAPE:
                lookForEndOfEscape(currentChar);
                return;
            case LOOKING_FOR_END_OF_COMMENT:
                lookForEndOfComment(currentChar);
                return;
            case LOOKING_FOR_NAME:
                lookForName(currentChar);
                return;
            case LOOKING_FOR_TOKEN_TYPE:
                lookForTokenType(currentChar);
                return;
            case LOOKING_FOR_END_OF_VALUE:
                lookForEndOfValue(currentChar);
                return;
            default:
                throw new IllegalStateException(""+currentState);
        }
    }

    protected void addLiteral() {
        if (currentName.length() <= 0) {
            return;
        }
        Literal literalToken = new Literal();
        literalToken.setParent(currentStructure);
        literalToken.setName(currentName);
        literalToken.setComment(currentComment);
        currentStructure.add(literalToken);
        currentComment = "";
        currentName = "";
    }

    protected void lookForEndOfValue(char currentChar) {
        if (currentChar == ' ') {
            return;
        }

        if (currentChar == '\t') {
            return;
        }

        if (currentChar == '\n') {
            currentState = ParsingState.LOOKING_FOR_NAME;
            return;
        }

        if (currentChar == '}') {
            currentStructure = currentStructure.getParent();
            currentState = ParsingState.LOOKING_FOR_NAME;
            return;
        }

        if (currentChar == '"') {
            stateStack.addFirst(currentState);
            currentState = ParsingState.LOOKING_FOR_END_OF_ESCAPE;
            return;
        }

        if (currentChar == '#') {
            currentComment = currentComment + "#";
            currentState = ParsingState.LOOKING_FOR_NAME;
            stateStack.addFirst(currentState);
            currentState = ParsingState.LOOKING_FOR_END_OF_COMMENT;
            return;
        }

        currentValue.setValue(currentValue.getValue() + String.valueOf(currentChar));

    }

    protected void lookForTokenType(char currentChar) {
        if (currentChar == ' ') {
            return;
        }

        if (currentChar == '\t') {
            return;
        }

        if (currentChar == '\n') {
            return;
        }

        if (currentChar == '{') {
            Structure currentChild = new Structure();
            currentChild.setName(currentName);
            currentChild.setComment(currentComment);
            currentChild.setParent(currentStructure);
            if (currentStructure != null) {
                currentStructure.add(currentChild);
            }
            currentStructure = currentChild;
            currentState = ParsingState.LOOKING_FOR_NAME;

            currentName = "";
            currentComment = "";
            return;

        }

        currentValue = new NamedValue();
        currentValue.setParent(currentStructure);
        currentValue.setName(currentName);
        currentValue.setComment(currentComment);
        currentValue.setValue(String.valueOf(currentChar));
        currentState = ParsingState.LOOKING_FOR_END_OF_VALUE;
        if (currentStructure != null) {
            currentStructure.add(currentValue);
        }

        currentName = "";
        currentComment = "";

        if (currentChar == '"') {
            stateStack.addFirst(currentState);
            currentState = ParsingState.LOOKING_FOR_END_OF_ESCAPE;
        }
        return;

    }

    protected void lookForEndOfEscape(char currentChar) {
        if (stateStack.peek() == ParsingState.LOOKING_FOR_NAME) {
            currentName = currentName + String.valueOf(currentChar);

        }

        if (stateStack.peek() == ParsingState.LOOKING_FOR_END_OF_VALUE) {
            currentValue.setValue(currentValue.getValue() + String.valueOf(currentChar));

        }

        if (currentChar == '"') {
            currentState = stateStack.remove();
            return;
        }
    }

    protected void lookForEndOfComment(char currentChar) {
        if (stateStack.peek() == ParsingState.LOOKING_FOR_NAME) {
            currentComment = currentComment + String.valueOf(currentChar);
        }

        if (currentChar == '\n') {
            currentState = stateStack.remove();
        }
    }

    protected void lookForName(char currentChar) {
        if (currentChar == '\n' || currentChar == ' ' || currentChar == '\t') {

            addLiteral();
            return;
        }

        if (currentChar == '{') {
            //Nameless array
            Structure newStructure = new Structure();
            newStructure.setParent(currentStructure);
            currentStructure.add(newStructure);
            currentStructure = newStructure;
            return;
        }

        if (currentChar == '"') {
            currentName = currentName + String.valueOf(currentChar);
            stateStack.addFirst(currentState);
            currentState = ParsingState.LOOKING_FOR_END_OF_ESCAPE;
            return;
        }

        if (currentChar == '#') {
            currentComment = currentComment + "#";
            stateStack.addFirst(currentState);
            currentState = ParsingState.LOOKING_FOR_END_OF_COMMENT;
            return;
        }

        if (currentChar == '=') {
            currentState = ParsingState.LOOKING_FOR_TOKEN_TYPE;
            return;
        }

        if (currentChar == '}') {
            addLiteral();
            currentStructure = currentStructure.getParent();
            return;
        }

        currentName = currentName + String.valueOf(currentChar);
        return;
    }


    public static Token parse(File file) throws Exception {
        try (FileInputStream fais = new FileInputStream(file)){
            return Parser.parse(fais);

        }
    }

    public static Token parse(InputStream inputStream) throws IOException{
        return parse(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
    }

    public static Token parse(Reader reader) throws IOException {
        Parser parser = new Parser();
        char[] currentChar = new char[1];
        int i = 0;
        while(i != -1) {
            i = reader.read(currentChar, 0, 1);
            if (i > 0 && currentChar[0] != '\r') {
                parser.parseNext(currentChar[0]);
            }
        }

        return parser.rootStructure;
    }


}
