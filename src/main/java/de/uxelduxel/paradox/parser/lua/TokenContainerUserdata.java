package de.uxelduxel.paradox.parser.lua;

import de.uxelduxel.paradox.parser.model.Token;
import de.uxelduxel.paradox.parser.model.TokenContainer;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaUserdata;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import java.util.Iterator;

public class TokenContainerUserdata extends TokenUserdata {

    private static final LuaValue ADD_F = new TwoArgFunction() {
        @Override
        public LuaValue call(LuaValue arg1, LuaValue arg2) {
            TokenContainer tokenContainer = (TokenContainer) arg1.checkuserdata(TokenContainer.class);
            tokenContainer.add((Token) arg2.checkuserdata(Token.class));
            return NIL;
        }
    };

    private static final LuaValue ITER_F = new VarArgFunction() {
        @Override
        public Varargs invoke(Varargs args) {
            final TokenContainer tokenContainer = (TokenContainer) args.checkuserdata(1, TokenContainer.class);
            Iterator<Token> tk = tokenContainer.getTokens().iterator();

            return varargsOf(new VarArgFunction() {
                @Override
                public Varargs invoke(Varargs args) {
                    Iterator iter = (Iterator) args.arg1().checkuserdata(Iterator.class);
                    if (!iter.hasNext()) {
                        return NIL;
                    }

                    Object o = iter.next();
                    if (o instanceof TokenContainer) {
                        return new TokenContainerUserdata((TokenContainer) o);
                    }

                    if (o instanceof Token) {
                        return new TokenUserdata((Token) o);
                    }

                    return error("invalid iterator");
                }
            }, new LuaUserdata(tk));
        }
    };

    private static final LuaValue UNPACK_F = new TwoArgFunction() {
        @Override
        public LuaValue call(LuaValue arg1, LuaValue arg2) {
            TokenContainer tokenContainer = (TokenContainer) arg1.checkuserdata(TokenContainer.class);
            LuaTable table = new LuaTable();

            String n = arg2.checkjstring();
            for (Token t : tokenContainer.getTokens()) {
                if (!n.equals(t.getName())) {
                    continue;
                }

                if (t instanceof TokenContainer) {
                    table.set(table.rawlen()+1, new TokenContainerUserdata((TokenContainer) t));
                } else {
                    table.set(table.rawlen()+1, new TokenUserdata(t));
                }
            }

            return table;
        }
    };

    private static final LuaValue GET_VALUE_F = new VarArgFunction() {
        @Override
        public Varargs invoke(Varargs args) {
            LuaValue value = GET_F.invoke(args).arg1();
            if (value.isnil()) {
                return NIL;
            }

            return value.get(VALUE);
        }
    };

    private static final LuaValue GET_F = new VarArgFunction() {
        @Override
        public Varargs invoke(Varargs args) {
            TokenContainer tokenContainer = (TokenContainer) args.checkuserdata(1, TokenContainer.class);
            Token token = tokenContainer;
            for (int i = 2; i <= args.narg(); i++) {
                if (tokenContainer == null) {
                    return NIL;
                }

                if (args.type(i) == LuaValue.TNUMBER) {
                    token = tokenContainer.get(args.checkint(i)-1);
                } else {
                    token = tokenContainer.get(args.checkjstring(i));
                }

                tokenContainer = null;

                if (token instanceof TokenContainer) {
                    tokenContainer = (TokenContainer) token;
                }
            }

            if (token instanceof TokenContainer) {
                return new TokenContainerUserdata((TokenContainer) token);
            }
            return token == null ? NIL : new TokenUserdata(token);
        }
    };

    private static final LuaValue REMOVE_F = new TwoArgFunction() {
        @Override
        public LuaValue call(LuaValue arg1, LuaValue arg2) {
            TokenContainer tokenContainer = (TokenContainer) arg1.checkuserdata(TokenContainer.class);
            tokenContainer.remove((Token) arg2.checkuserdata(Token.class));
            return NIL;
        }
    };


    private static final LuaValue ADD = valueOf("add");

    private static final LuaValue GET = valueOf("get");

    private static final LuaValue GET_VALUE = valueOf("getValue");

    private static final LuaValue REMOVE = valueOf("remove");

    private static final LuaValue UNPACK = valueOf("unpack");

    private static final LuaValue ITER = valueOf("iter");



    private TokenContainer container;


    public TokenContainerUserdata(TokenContainer token) {
        super(token);
        this.container = token;
    }

    public void setTablePath(LuaValue key, LuaValue value) {
        TokenContainer tokenContainer = container;
        int len = key.length();
        for (int i = 1; i < len; i++) {
            LuaValue lv = key.get(i);

            if (lv.isnil()) {
                break;
            }

            Token token;
            if (lv.type() == LuaValue.TNUMBER) {
                token = tokenContainer.get(lv.checkint()-1);
            } else {
                token = tokenContainer.get(lv.checkjstring());
            }


            if (token instanceof TokenContainer) {
                tokenContainer = (TokenContainer) token;
                continue;
            }

            return;
        }

        LuaValue lv = key.get(len);
        if (lv.isnil()) {
            return;
        }

        if (lv.type() == LuaValue.TNUMBER) {
            tokenContainer.set(lv.checkint()-1, value.isnil() ? null : (Token) value.checkuserdata(Token.class));
        } else {
            Token token = tokenContainer.get(lv.checkjstring());
            if (token != null) {
                tokenContainer.remove(token);
            }

            if (value.isnil()) {
                return;
            }

            tokenContainer.add((Token) value.checkuserdata(Token.class));
        }
    }

    public LuaValue getTablePath(LuaValue key) {
        TokenContainer tokenContainer = container;
        Token token = container;
        int len = key.length();
        for (int i = 1; i <= len; i++) {
            if (tokenContainer == null) {
                return NIL;
            }

            LuaValue lv = key.get(i);

            if (lv.isnil()) {
                break;
            }

            if (lv.type() == LuaValue.TNUMBER) {
                token = tokenContainer.get(lv.checkint()-1);
            } else {
                token = tokenContainer.get(lv.checkjstring());
            }

            tokenContainer = null;

            if (token instanceof TokenContainer) {
                tokenContainer = (TokenContainer) token;
            }
        }

        if (token instanceof TokenContainer) {
            return new TokenContainerUserdata((TokenContainer) token);
        }

        return token == null ? NIL : new TokenUserdata(token);
    }

    @Override
    public LuaValue get(LuaValue key) {
        if (key.isnumber()) {
            Token tok = container.get(key.checkint()-1);
            if (tok == null) {
                return NIL;
            }

            if (tok instanceof TokenContainer) {
                return new TokenContainerUserdata((TokenContainer) tok);
            }

            return new TokenUserdata(tok);
        }

        if (key.istable()) {
            return getTablePath(key);
        }

        if (ADD.eq_b(key)) {
            return ADD_F;
        }

        if (GET.eq_b(key)) {
            return GET_F;
        }

        if (REMOVE.eq_b(key)) {
            return REMOVE_F;
        }

        if (UNPACK.eq_b(key)) {
            return UNPACK_F;
        }

        if (ITER.eq_b(key)) {
            return ITER_F;
        }

        if (GET_VALUE.eq_b(key)) {
            return GET_VALUE_F;
        }

        if (key.isstring()) {
            String str = key.checkjstring();
            Token token = container.get(str);
            if (token == null && str.startsWith("_")) {
                String sub = str.substring(1);
                token = container.get(sub);
                if (token == null) {
                    try {
                        token = container.get(Integer.parseInt(sub)-1);
                    } catch (NumberFormatException exc) {

                    }
                }
            }


            if (token == null) {
                return super.get(key);
            }

            if (token instanceof TokenContainer) {
                return new TokenContainerUserdata((TokenContainer) token);
            }

            return new TokenUserdata(token);
        }

        return super.get(key);
    }

    @Override
    public LuaValue len() {
        return valueOf(container.getTokens().size());
    }

    @Override
    public void set(LuaValue key, LuaValue value) {
        if (key.isnumber()) {
            container.set(key.checkint() -1, (Token) value.checkuserdata(Token.class));
            return;
        }

        if (key.istable()) {
            setTablePath(key, value);
            return;

        }


        super.set(key, value);
    }
}
