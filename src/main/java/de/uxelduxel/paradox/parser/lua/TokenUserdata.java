package de.uxelduxel.paradox.parser.lua;

import de.uxelduxel.paradox.parser.model.NamedValue;
import de.uxelduxel.paradox.parser.model.Token;
import de.uxelduxel.paradox.parser.model.TokenContainer;
import org.luaj.vm2.LuaUserdata;
import org.luaj.vm2.LuaValue;

public class TokenUserdata extends LuaUserdata {

    private Token token;

    protected static LuaValue NAME = valueOf("name");
    protected static LuaValue TYPE = valueOf("type");
    protected static LuaValue VALUE = valueOf("value");
    private static final LuaValue PARENT = valueOf("parent");

    public TokenUserdata(Token obj) {
        super(obj);
        token = obj;
    }

    @Override
    public LuaValue get(LuaValue key) {
        if (PARENT.eq_b(key)) {
            TokenContainer tc = token.getParent();
            return tc == null ? NIL : new TokenContainerUserdata(tc);
        }

        if (NAME.eq_b(key)) {
            String n = token.getName();
            return n == null ? NIL : valueOf(n);
        }

        if (TYPE.eq_b(key)) {
            return valueOf(token.type().name().toLowerCase());
        }

        if (VALUE.eq_b(key)) {
            if (token instanceof NamedValue) {
                String str = ((NamedValue) token).getValue();
                return str == null ? NIL : valueOf(str);
            }

            return NIL;
        }

        return NIL;
    }

    @Override
    public void set(LuaValue key, LuaValue value) {
        if (PARENT.eq_b(key)) {
            token.setParent(value.isnil() ? null : (TokenContainer) value.checkuserdata(TokenContainer.class));
            return;
        }

        if (NAME.eq_b(key)) {
            token.setName(value.isnil() ? null : value.checkjstring());
            return;
        }

        if (VALUE.eq_b(key) && token instanceof NamedValue) {
            ((NamedValue) token).setValue(value.isnil() ? null : value.checkjstring());
        }

        if (TYPE.eq_b(key)) {
            error("cant change token type");
        }
    }
}
