package de.uxelduxel.paradox.parser.lua;

import de.uxelduxel.paradox.parser.Parser;
import de.uxelduxel.paradox.parser.model.*;
import org.luaj.vm2.LuaString;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.TwoArgFunction;
import org.luaj.vm2.lib.VarArgFunction;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;

public class ParadoxParserLib extends TwoArgFunction {
    @Override
    public LuaValue call(LuaValue mn, LuaValue env) {

        LuaValue t = new LuaTable();
        t.set("parse", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                return parseFile(args);
            }
        });

        t.set("parseString", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                return parseString(args);
            }
        });

        t.set("newLiteral", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                return newLiteral(args);
            }
        });

        t.set("newValue", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                return newValue(args);
            }
        });

        t.set("newRoot", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                return newRoot(args);
            }
        });

        t.set("newArray", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                return newArray(args);
            }
        });

        t.set("newSection", new VarArgFunction() {
            @Override
            public Varargs invoke(Varargs args) {
                return newSection(args);
            }
        });

        env.checkglobals().package_.setIsLoaded("hoi4", t.checktable());

        return t;
    }

    protected Varargs newLiteral(Varargs args) {
        Literal literal = new Literal();
        literal.setName(args.optjstring(1, null));
        literal.setParent((TokenContainer) args.optuserdata(2, TokenContainer.class, null));
        return new TokenUserdata(literal);
    }

    protected Varargs newValue(Varargs args) {
        NamedValue literal = new NamedValue();
        literal.setName(args.optjstring(1, null));
        literal.setName(args.optjstring(2, null));
        literal.setParent((TokenContainer) args.optuserdata(3, TokenContainer.class, null));
        return new TokenUserdata(literal);
    }

    protected Varargs newRoot(Varargs args) {
        return new TokenContainerUserdata(new Structure());
    }

    protected Varargs newArray(Varargs args) {
        Structure literal = new Structure();
        literal.setParent((TokenContainer) args.optuserdata(1, TokenContainer.class, null));
        return new TokenContainerUserdata(literal);
    }

    protected Varargs newSection(Varargs args) {
        Structure literal = new Structure();
        literal.setName(args.optjstring(1, null));
        literal.setParent((TokenContainer) args.optuserdata(2, TokenContainer.class, null));
        return new TokenContainerUserdata(literal);
    }

    protected Varargs parseString(Varargs args) {
        LuaString str = args.checkstring(1);
        Token tk;
        try {
            tk = Parser.parse(new ByteArrayInputStream(str.m_bytes, str.m_offset, str.m_length));
        } catch (Exception e) {
            return varargsOf(NIL, valueOf(e.getMessage()));
        }

        if (tk instanceof TokenContainer) {
            return new TokenContainerUserdata((TokenContainer) tk);
        }

        return new TokenUserdata(tk);

    }

    protected Varargs parseFile(Varargs args) {
        File f = new File(args.checkjstring(1));
        Token tk;
        try {
            tk = Parser.parse(f);
        } catch (Exception e) {
            return varargsOf(NIL, valueOf(e.getMessage()));
        }

        if (tk instanceof TokenContainer) {
            return new TokenContainerUserdata((TokenContainer) tk);
        }

        return new TokenUserdata(tk);
    }
}
